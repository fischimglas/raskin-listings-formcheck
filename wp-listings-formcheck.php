<?php
/**
 * Plugin Name: Raskin WP-Listings Formcheck
 * Description: Checks form for label content (max. xxx) and prevents form from submitting if content is too long.
 * Version: 0.1
 * Last Update: 2019-04-04
 * Author: Jam
 * Author URI: https://hashcookie.ch
 **/


add_action('wp_enqueue_scripts','addFormCheckJs');

function addFormCheckJs() {
	wp_enqueue_script( 'scrollto', plugins_url( '/jquery.scrollTo.min.js', __FILE__ ), array('jquery'));
	wp_enqueue_script( 'lodash', plugins_url( '/lodash.min.js', __FILE__ ));
	wp_enqueue_script( 'formcheck', plugins_url( '/formcheck.js', __FILE__ ), array('jquery'));
}