jQuery(function() {
  var form = jQuery('form#submit-job-form');

  /**
   *
   */
  function validateForm() {
    var returnValue = true;
    // Form exists?
    // find the fields which have a maximum length
    form.find('label').each(function(index, label) {
      var formData = form.serializeArray();
      label = jQuery(label);
      var labelText = label.text();
      var max = labelText.match(/max\. (\d+)/);
      if(label !== null && max) {
        var maxLen = max[1];
        // get input field and check length
        var field = label.parent().find('textarea').first();
        if(!field) {
          field = label.parent().find('input').first();
        }
        // In case we find no field
        if(!field) {
          console.warn('Input field for label ', labelText, ' could not be found, field skipped.');
        } else {
          var fieldName = field.attr('name');
          // Read Value of Input field. Prioritize tinyMCE content directly, fallback for regular input content retrieval
          var fieldValue = null;
          var tinyMceInstance = _.find(tinymce.get(), {id: fieldName});
          if(tinyMceInstance) {
            fieldValue = tinyMceInstance.getContent({format: 'text'});
          } else {
            var valueEl = _.find(formData, {name: fieldName});
            if(valueEl) {
              fieldValue = jQuery('<p>' + valueEl.value + '</p>').text();
            }
          }
//           console.log('label:', text);
//           console.log('max:', max[1]);
//           console.log('field:', fieldName);
//           console.log('value:', fieldValue);
//           console.log('----');
          var len = fieldValue.length;
          if(len > maxLen) {
//            console.log('name:', fieldName);
//            console.log('field has too much content:', len - maxLen);
//            console.log('scroll to', label.offset().top)
            jQuery('body').scrollTo(label.offset().top - 120);
            returnValue = false;
          }
        }
      }
    });
    // return false;
    return returnValue;
  }

  if(form) {
    form.submit(function(e) {
      if(!validateForm()) {
        e.preventDefault();
        e.stopPropagation();
      }
    });
  }
});